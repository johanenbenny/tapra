*** Settings ***

*** Variables ***
${popUpVoorwaardenbuttonAccepteren}  xpath://button/span[text()='Accepteren']
${InputZoeken}  id:searchfor
${ButtonZoeken}  xpath://button[@class='wsp-search__btn']


*** Keywords ***
Inloggen
    [Arguments]     ${gebruikersnaam}  ${wachtwoord}
    # Open Browser    ${LOGIN_URL}    ${BROWSER}
    # Maximize Browser Window
    Create Chrome Webdriver
    Go To  ${LOGIN_URL}
    Title Should Be  Portaal
    Click Link  Inloggen
    Page Should Contain     username
    Page Should Contain     password
    Voer Gebruikersnaam In  ${gebruikersnaam}
    Voer Wachtwoord In  ${wachtwoord}
    Click Button    name:submit
    Wait Until Page Contains  Welkom op het EDSN Portaal
    Page Should Contain    Welkom op het EDSN Portaal
    Uitbreiden Log Bestand  \nSuccesvol ingelogd met gebruiker: ${gebruikersnaam}.

Voer Gebruikersnaam In
    [Arguments]     ${gebruikersnaam}
    Input Text  username    ${gebruikersnaam}

Voer Wachtwoord In
    [Arguments]     ${wachtwoord}
    Input Text  password  ${wachtwoord}

Controleren Versienummer
    ${versienummer} =  Get Text  ${versienummerLocator}
    Uitbreiden Log Bestand  Het versienummer in het Portaal is: ${versienummer}

Create Chrome Webdriver
    ${CHROME_OPTIONS}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    # ${PREFS}    Create Dictionary    download.default_directory=${TEMP_DOWNLOADS_DIR}    safebrowsing.enabled=false
    ${PREFS}    Create Dictionary     safebrowsing.enabled=false
    # Call Method    ${CHROME_OPTIONS}    add_argument    --start-maximized
    Call Method    ${CHROME_OPTIONS}    add_experimental_option    prefs    ${PREFS}
    Create Webdriver    ${BROWSER}    chrome_options=${CHROME_OPTIONS}