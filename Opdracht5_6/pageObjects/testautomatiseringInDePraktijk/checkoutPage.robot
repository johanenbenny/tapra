*** Settings ***
Resource    ../../Resources/resource.robot


*** Variables ***
${FirstNameInput}     id:billing_first_name
${LastNameInput}      id:billing_last_name
${StreetInput}        id:billing_address_1
${PostcodeZipInput}   id:billing_postcode
${TownCityInput}      id:billing_city
${PhoneInput}         id:billing_phone
${EmailAddressInput}  id:billing_email
${PlaceOrderButton}   id:place_order


*** Keywords ***