*** Settings ***
Resource  ../pageObjects/bol/homePage.robot
Resource  ../pageObjects/testAutomatiseringInDePraktijk/homePage.robot
Resource  ../pageObjects/testAutomatiseringInDePraktijk/homePage.robot
Resource  ../pageObjects/testAutomatiseringInDePraktijk/bomenPage.robot
Resource  ../pageObjects/testAutomatiseringInDePraktijk/cartPage.robot
Resource  ../pageObjects/testAutomatiseringInDePraktijk/checkoutPage.robot
Library  SeleniumLibrary

*** Variables ***
${BROWSER}      Chrome

*** Keywords ***
Open Pagina
    [Arguments]  ${url}

    Create Webdriver  Chrome
    Go To  ${url}
