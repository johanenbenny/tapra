*** Settings ***
Resource  ../Resources/resource.robot
Library  	SeleniumLibrary

*** Variables ***

*** Test Cases ***
Opdracht2
    Open Pagina  https://nos.nl
    Title Should Be  NOS.nl - Nieuws, Sport en Evenementen | Nederlandse Omroep Stichting
    Close Browser

Opdracht3
    Open Pagina  https://bol.com
    Wait Until Element Is Visible    ${popUpVoorwaardenbuttonAccepteren}
    Click Element    ${popUpVoorwaardenbuttonAccepteren}
    Input Text  ${InputZoeken}  koffiemachine
    Click Button    ${ButtonZoeken}
    Page Should Contain  Dit zijn de resultaten voor
    Close Browser

Opdracht4
    Open Pagina  http://testautomatiseringindepraktijk.testvisie.nl/
    Mouse Over  ${menuProducten}
    Click Element  ${menuProductenSubitemParasols}
    Title Should Be  Parasols – The shadow company
    Close Browser

Opdracht6
    Open Pagina  http://testautomatiseringindepraktijk.testvisie.nl/
    Mouse Over  ${menuProducten}
    Click Element  ${menuProductenSubitemBomen}
    Click Element  ${kleinsteBoom}
    Input Text  ${aantalInput}  2
    Click Element  ${addToCartButton}
    Page Should Contain  have been added to your cart
    Click Element  ${menuCart} 
    Page Should Contain  1,207.58
    Click Element  ${ProceedToCheckoutButton}
    Input Text    ${FirstNameInput}  Jan
    Input Text    ${LastNameInput}   Jansen
    Input Text    ${StreetInput}  Hoofdstraat}
    Input Text    ${PostcodeZipInput}   1234 ab
    Input Text    ${TownCityInput}  Amsterdam
    Input Text    ${PhoneInput}    01234556789
    Input Text    ${EmailAddressInput}  a@b.nl
    Mouse Over    ${PlaceOrderButton}
    Click Element  ${PlaceOrderButton}
    Wait Until Page Contains  Order received


