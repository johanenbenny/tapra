*** Settings ***
Library     SeleniumLibrary
#Suite Teardown    Close Browser
Resource    HomePage.resource
Resource    generieke resources.resource
Resource    cartPage.resource
Resource    BomenPage.resource

Test Setup    Open Browser    browser=gc

***Test Cases***
Open nos.nl
    Open browser    https://nos.nl    gc
    Title Should Be    NOS.nl - Nieuws, Sport en Evenementen | Nederlandse Omroep Stichting
    Close Browser

open bol.com
    Open Browser    https://bol.com    gc
    Maximize Browser Window
    Wait Until Element Is Visible    //button/span[text()='Accepteren']
    Click Element    //button/span[text()='Accepteren']
    Input Text    searchfor    koffiemachine
    Click Element    //button[@data-analytics-id="px_search_button_click"]
    Page Should Contain    Dit zijn de resultaten voor
    #Page Should Contain    Dit zijn de resultaten voor koffiemachine

parasols
    Open Browser    http://testautomatiseringindepraktijk.testvisie.nl/    gc
    Mouse Over    menu-item-70
#    Mouse Down    menu-item-72
    Click Element    menu-item-72

opdracht 6 bomen bestellen
    open Home Page
    navigate to product category    Bomen
    Voeg toe aan winkelwagen en open cart    groteBoom
    navigate to cart van bomen
    # pas hoeveelheid aan     3
    # controleer prijs
    #ga naar bestelling
    #vul in klantgegevens    klant1
    #plaats bestelling
    